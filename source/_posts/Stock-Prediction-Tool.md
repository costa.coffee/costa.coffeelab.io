---
title: UK Youth Unemployment Plotting Tool
date: 2019-11-03 23:56:23
category: projects
---

I've worked with Movement to Work on this project. It was at JP Morgan Code For Good event 2019.

![](https://www.movementtowork.com/wp-content/uploads/2018/08/MTW_Logo.png)

> Movement to Work inspires young people to change their lives through positive encounters with work.

The company wanted to be able to analyze a large amount of data on Youth Unemployment of [Wards](https://en.wikipedia.org/wiki/Wards_and_electoral_divisions_of_the_United_Kingdom "Wikipedia Description of Wards") in UK. 
- There are 9,456 Wards in United Kingdom (2014).

>___The image below displays wards in United Kingdom___
![](https://i.ibb.co/ykHZjYv/download.png)

Once we've plotted the wards on our map we had to combine it with a dataset of Benefit Claimant of 18-24 yo's [Source](www.nomisweb.co.uk/query "Where we took the data.")

>___UK Benefit Claimant of 18-24 Year Olds___
![](https://media.giphy.com/media/QW9myZBRhuMiJlLfg3/giphy.gif)

All the maps were wrapped in CSS and HTML with Bootstrap framework. All script execution is done serverside the data loads ___instantly___! 

The Webpage also featured mobile version. Which worked just as well.

>___Mobile View___
![](https://i.ibb.co/NxsRRwX/Screenshot-1.jpg)