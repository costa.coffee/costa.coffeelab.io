---
title: 
date: 2019-10-25 14:19:05
---

> ## More about me:

 >  I was born in Minsk, Belarus.<br>
My first language is Russian however after living in UK from the age of 12 I have fluently grasped English.<br>
I love exploring new places, trying new foods and indulging myself into new cultures. <br>
Sports hobbies: Tennis, Skateboarding, Sailing and <br> Snooker (if you can call it a sport).
Other hobbies: Electronics, Gaming, Photography and making 11/10 sandwiches.


> ##### My coding abilities:

Language         | My Ability
  -------------  | -------------
  HTML (5)       | ⭐⭐⭐⭐⭐
  CSS            | ⭐⭐⭐⭐⭐
  Java           | ⭐⭐⭐⭐
  MySQL          | ⭐⭐⭐⭐
  JavaScript     | ⭐⭐⭐⭐
  Python         | ⭐⭐⭐⭐
  PHP            | ⭐⭐⭐ 
  NodeJS         | ⭐
  Ruby on Rails  | ⭐

#### My other abilities:
Language         | My Ability
  -------------  | -------------
  Adobe Photoshop | ⭐⭐⭐⭐⭐
  Adobe Fireworks | ⭐⭐⭐⭐⭐
  Microsoft Office | ⭐⭐⭐⭐⭐
  Figma           | ⭐⭐⭐⭐
